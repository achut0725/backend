const express = require('express')
var cors = require('cors')
const app = express()
const port = 3000

const { Model } = require('./model')
const model = new Model()

// let seats = [false,false,false,false,false,false,false,false,false,false,]

app.use(cors())

app.get ('/', (req, res) => {
    res.send('Hello World !')
})

app.get ('/about', (req, res) => {
    res.send('about app.get')
})


app.get ('/seats', (req, res) => {
    res.json({})
})

app.listen (port, () => {
    console.log('Example app listening on port ${port}')
})